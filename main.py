menu_input = 22
garbanzos = [0, 0]
lentejas = [0, 0]
arvejas = [0, 0]
#en la posicion 0 se almacena la cantidad, en la 2 se almacena el precio
print("\t\t Granos.Inc \nPresione el numero para realizar su operacion\n")

def ingresar_granos(): #opcion 1
    print("\nCantidad de garbanzos: ")
    garbanzos[0] = input()
    
    print("\nCantidad de lentejas: ")
    lentejas[0] = input()
    
    print("\nCantidad de arvejas: ")
    arvejas[0] = input()
    
    print("En inventario se tiene " + garbanzos[0] + " garbanzos, " + lentejas[0] + ' lentejas y ' + arvejas[0] + " arvejas")

    print("Para volver al menu presione 0")
    menu_input=input()
    if menu_input == '0':
        menu()
    
def costo_granos():
    x = (garbanzos[1]*garbanzos[0])
    y = (lentejas[1]*lentejas[0])
    z = (arvejas[1]*arvejas[0])
    
    print("\nCosto de garbanzos: ")
    garbanzos[1] = input()
    
    print("\nCosto de lentejas: ")
    lentejas[1] = input()
    
    print("\nCosto de arvejas: ")
    arvejas[1] = input()
    
    print("El costo de cada grano es " + x + "$ garbanzos, " + y + "$ lentejas y " + z + "$ arvejas")

    print("Para volver al menu presione 0")
    menu_input=input()
    if menu_input == '0':
        menu()

def costo_total():
    x = (arvejas[1]*arvejas[0]) + (lentejas[1]*lentejas[0]) + (garbanzos[1]*garbanzos[0])
    print("\nEl costo total de todos los granos es" + x)

    print("Para volver al menu presione 0")
    menu_input=input()
    if menu_input == '0':
        menu()


def menu(): #menu principal del programa
    print("1.Ingresar grano\n2.Calcular costo por tipo de grano\n3.Calcular costo total\n4.Terminar")
    menu_input = input()
    if menu_input == '1':
        ingresar_granos()
    elif menu_input=='2':
        costo_granos()
    elif menu_input=='3':
        costo_total()
    else:
        return 0 
        
menu()